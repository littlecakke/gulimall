package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:00:44
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity>
{
}
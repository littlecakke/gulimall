package com.atguigu.gulimall.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author GRS
 * @since 2023/10/2 23:12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttrGroupRelationVo
{
    private Long attrId;

    private Long attrGroupId;
}
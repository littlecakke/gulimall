package com.atguigu.gulimall.product.exception;

import com.atguigu.common.exception.BizCodeEnum;
import com.atguigu.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * @author GRS
 * @since 2023/9/24 10:16
 */
@RestControllerAdvice(basePackages = "com.atguigu.gulimall.product.controller")
@Slf4j
public class ExceptionControllerAdvice
{
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R validationExceptionHandler(MethodArgumentNotValidException e)
    {
        log.error("数据校验出现问题：{}，异常类型：{}", e.getMessage(), e.getClass());

        Map<String, String> responseMap = new HashMap<>();

        BindingResult result = e.getBindingResult();
        result
            .getFieldErrors()
            .forEach(item -> responseMap.put(item.getField(), item.getDefaultMessage()));

        return R
            .error(
                BizCodeEnum.VALID_EXCEPTION.getCode(),
                BizCodeEnum.VALID_EXCEPTION.getMsg()
            )
            .put("data", responseMap);
    }

    @ExceptionHandler(value = Throwable.class)
    public R exceptionHandler(Throwable throwable)
    {
        log.error("异常：{}", throwable.getMessage());
        return R.error(
            BizCodeEnum.UNKNOWN_EXCEPTION.getCode(),
            BizCodeEnum.UNKNOWN_EXCEPTION.getMsg()
        );
    }
}
package com.atguigu.gulimall.product.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author GRS
 * @since 2023/10/4 14:34
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BrandVo
{
    private Long brandId;

    private String brandName;
}
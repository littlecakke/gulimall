package com.atguigu.gulimall.product.entity;

import com.atguigu.common.validation.AddGroup;
import com.atguigu.common.validation.ListValue;
import com.atguigu.common.validation.UpdateGroup;
import com.atguigu.common.validation.UpdateStatusGroup;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;

/**
 * 品牌
 * 
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:00:44
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable
{
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
	@TableId
	@NotNull(message = "修改必须指定品牌ID", groups = { UpdateGroup.class })
	@Null(message = "新增不能指定ID", groups = { AddGroup.class })
	private Long brandId;

	/**
	 * 品牌名
	 */
	@NotBlank(message = "品牌名必须提交", groups = { UpdateGroup.class, AddGroup.class })
	private String name;

	/**
	 * 品牌logo地址
	 */
	@NotBlank(groups = { AddGroup.class })
	@URL(message = "Logo必须是一个合法的URL地址", groups = { UpdateGroup.class, AddGroup.class })
	private String logo;

	/**
	 * 介绍
	 */
	@NotEmpty(message = "介绍必须提交")
	private String descript;

	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	@NotNull(groups = { AddGroup.class, UpdateStatusGroup.class })
	@ListValue(values = { 0, 1 }, groups = { AddGroup.class, UpdateStatusGroup.class })
	private Integer showStatus;

	/**
	 * 检索首字母
	 */
	@NotEmpty(message = "检索首字母必须提交", groups = { AddGroup.class })
	@Pattern(regexp = "^[a-zA-Z]$", message = "检索首字母必须是一个英文字母", groups = { UpdateGroup.class, AddGroup.class })
	private String firstLetter;

	/**
	 * 排序
	 */
	@NotNull(message = "排序必须提交", groups = { AddGroup.class })
	@Min(value = 0, message = "排序字段的值必须为一个自然数", groups = { UpdateGroup.class, AddGroup.class })
	private Integer sort;
}
package com.atguigu.gulimall.product.vo;

import lombok.Data;

import java.util.List;

/**
 * @author GRS
 * @since 2023/9/26 18:55
 */
@Data
public class AttrResponseVo extends AttrVo
{
    /**
     * 所属分类名
     */
    private String categoryName;

    /**
     * 所属分组名
     */
    private String groupName;

    /**
     * 分类路径
     */
    private List<Long> catelogPath;
}
package com.atguigu.gulimall.product.service.impl;

import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:00:44
 */
@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService
{
    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params)
    {
        IPage<CategoryEntity> page = this.page(
            new Query<CategoryEntity>().getPage(params),
            new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree()
    {
        // 查出所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);

        // 组装为父子树形结构
        // 找到所有的一级分类
        return entities
            .stream()
            .filter(item -> item.getParentCid() == 0)
            .peek(item -> item.setChildren(getChildren(item, entities)))
            .sorted(Comparator.comparingInt(item -> (item.getSort() == null ? 0 : item.getSort())))
            .collect(Collectors.toList());
    }

    @Override
    public void removeMenuByIds(List<Long> idList)
    {
        // TODO 检查当前删除的菜单，是否被别的地方引用

        // 逻辑删除
        baseMapper.deleteBatchIds(idList);
    }

    @Override
    public List<Long> findCatelogPath(Long catelogId)
    {
        List<Long> parentPath = findParentPath(catelogId, new ArrayList<>());

        Collections.reverse(parentPath);

        return parentPath;
    }

    /**
     * 级联更新所有关联数据
     * @param category 前端传入的实体类
     */
    @Transactional
    @Override
    public void updateCascader(CategoryEntity category)
    {
        this.updateById(category);
        categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());
    }

    // 递归查找所有菜单的子菜单
    private List<CategoryEntity> getChildren(CategoryEntity root, List<CategoryEntity> all)
    {
        return all
            .stream()
            .filter(categoryEntity -> root.getCatId().equals(categoryEntity.getParentCid()))
            .peek(item -> item.setChildren(getChildren(item, all)))
            .sorted(Comparator.comparingInt(menu -> (menu.getSort() == null ? 0 : menu.getSort())))
            .collect(Collectors.toList());
    }

    private List<Long> findParentPath(Long catelogId, List<Long> paths)
    {
        // 收集当前节点ID
        paths.add(catelogId);

        CategoryEntity category = this.getById(catelogId);

        if (category.getParentCid() != 0)
        {
            findParentPath(category.getParentCid(), paths);
        }
        return paths;
    }
}
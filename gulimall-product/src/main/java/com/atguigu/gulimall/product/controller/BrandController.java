package com.atguigu.gulimall.product.controller;

import java.util.Arrays;
import java.util.Map;

import com.atguigu.common.validation.AddGroup;
import com.atguigu.common.validation.UpdateGroup;
import com.atguigu.common.validation.UpdateStatusGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;

/**
 * 品牌
 *
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:00:44
 */
@RestController
@RequestMapping("product/brand")
public class BrandController
{
    @Autowired
    private BrandService brandService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params)
    {
        PageUtils page = brandService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{brandId}")
    public R info(@PathVariable("brandId") Long brandId)
    {
		BrandEntity brand = brandService.getById(brandId);

        return R.ok().put("brand", brand);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@Validated({AddGroup.class}) @RequestBody BrandEntity brand)
    {
        brandService.save(brand);
        return R.ok();
    }

    /**
     * 修改
     */
    @PutMapping("/update")
    public R update(@Validated({UpdateGroup.class}) @RequestBody BrandEntity brand)
    {
		brandService.updateDetail(brand);

        return R.ok();
    }

    /**
     * 修改状态
     */
    @PutMapping("/update/status")
    public R updateStatus(@Validated(UpdateStatusGroup.class) @RequestBody BrandEntity brand)
    {
        brandService.updateById(brand);
        return R.ok();
    }

    /**
     * 删除
     */
    @DeleteMapping("/delete")
    public R delete(@RequestBody Long[] brandIds)
    {
		brandService.removeByIds(Arrays.asList(brandIds));

        return R.ok();
    }
}
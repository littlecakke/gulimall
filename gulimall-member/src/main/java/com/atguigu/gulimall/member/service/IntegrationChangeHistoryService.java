package com.atguigu.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.member.entity.IntegrationChangeHistoryEntity;

import java.util.Map;

/**
 * 积分变化历史记录
 *
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:11:48
 */
public interface IntegrationChangeHistoryService extends IService<IntegrationChangeHistoryEntity>
{
    PageUtils queryPage(Map<String, Object> params);
}
package com.atguigu.gulimall.member.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.member.dao.MemberStatisticsInfoDao;
import com.atguigu.gulimall.member.entity.MemberStatisticsInfoEntity;
import com.atguigu.gulimall.member.service.MemberStatisticsInfoService;

/**
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:11:48
 */
@Service("memberStatisticsInfoService")
public class MemberStatisticsInfoServiceImpl extends ServiceImpl<MemberStatisticsInfoDao, MemberStatisticsInfoEntity> implements MemberStatisticsInfoService
{

    @Override
    public PageUtils queryPage(Map<String, Object> params)
    {
        IPage<MemberStatisticsInfoEntity> page = this.page(
            new Query<MemberStatisticsInfoEntity>().getPage(params),
            new QueryWrapper<>()
        );

        return new PageUtils(page);
    }
}
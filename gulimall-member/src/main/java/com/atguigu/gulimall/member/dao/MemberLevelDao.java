package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:11:48
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity>
{
}
package com.atguigu.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.member.entity.MemberEntity;

import java.util.Map;

/**
 * 会员
 *
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:11:48
 */
public interface MemberService extends IService<MemberEntity>
{
    PageUtils queryPage(Map<String, Object> params);
}
package com.atguigu.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.member.entity.MemberReceiveAddressEntity;

import java.util.Map;

/**
 * 会员收货地址
 *
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:11:48
 */
public interface MemberReceiveAddressService extends IService<MemberReceiveAddressEntity>
{
    PageUtils queryPage(Map<String, Object> params);
}
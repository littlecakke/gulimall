package com.atguigu.gulimall.thirdparty;

import com.aliyun.oss.OSS;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author GRS
 * @since 2023/9/23 18:29
 */
@SpringBootTest
public class ApplicationTests
{
    @Autowired
    private OSS oss;

    @Test
    public void test1() throws IOException
    {
        InputStream inputStream = Files.newInputStream(Paths.get("D:\\Pictures\\表情包\\柴郡你这小孩.jpg"));

        oss.putObject("gulimall-littlecake", "test3.jpg", inputStream);

        oss.shutdown();
        inputStream.close();

        System.out.println("上传完成...");
    }
}
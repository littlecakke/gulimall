package com.atguigu.gulimall.thirdparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author GRS
 * @since 2023/9/23 18:06
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ThirdPartyApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(ThirdPartyApplication.class, args);
    }
}
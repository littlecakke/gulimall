package com.atguigu.gulimall.thirdparty.controller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.model.MatchMode;
import com.aliyun.oss.model.PolicyConditions;
import com.atguigu.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author GRS
 * @since 2023/9/23 19:36
 */
@RestController
@Slf4j
public class OssController
{
    @Autowired
    private OSS oss;

    @Value("${alibaba.cloud.oss.bucket}")
    private String bucket;

    @Value("${alibaba.cloud.oss.endpoint}")
    private String endpoint;

    @Value("${alibaba.cloud.access-key}")
    private String accessId;

    @RequestMapping("/oss/policy")
    public R policy()
    {
        Map<String, String> responseMap = null;

        String host = "https://" + bucket + "." + endpoint;
        String callbackUrl = "http://localhost";
        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        try
        {
            long expireTime = 30;
            long expireEndTime = System.currentTimeMillis() + expireTime * 1000;
            Date expiration = new Date(expireEndTime);
            PolicyConditions policyConditions = new PolicyConditions();
            policyConditions.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, 1048576000);
            policyConditions.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, "");

            String postPolicy = oss.generatePostPolicy(expiration, policyConditions);
            byte[] binaryData = postPolicy.getBytes(StandardCharsets.UTF_8);
            String encodedPolicy = BinaryUtil.toBase64String(binaryData);
            String postSignature = oss.calculatePostSignature(postPolicy);

            responseMap = new LinkedHashMap<>();
            responseMap.put("accessid", accessId);
            responseMap.put("policy", encodedPolicy);
            responseMap.put("signature", postSignature);
            responseMap.put("dir", format + "/");
            responseMap.put("host", host);
            responseMap.put("expire", String.valueOf(expireEndTime / 1000));
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
        }
        return R.ok().put("data", responseMap);
    }
}
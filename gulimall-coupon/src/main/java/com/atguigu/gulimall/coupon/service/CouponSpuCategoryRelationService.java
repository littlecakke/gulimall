package com.atguigu.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.coupon.entity.CouponSpuCategoryRelationEntity;

import java.util.Map;

/**
 * 优惠券分类关联
 *
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:09:59
 */
public interface CouponSpuCategoryRelationService extends IService<CouponSpuCategoryRelationEntity>
{
    PageUtils queryPage(Map<String, Object> params);
}
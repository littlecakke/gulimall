package com.atguigu.gulimall.coupon.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.coupon.dao.CouponDao;
import com.atguigu.gulimall.coupon.entity.CouponEntity;
import com.atguigu.gulimall.coupon.service.CouponService;

/**
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:09:59
 */
@Service("couponService")
public class CouponServiceImpl extends ServiceImpl<CouponDao, CouponEntity> implements CouponService
{

    @Override
    public PageUtils queryPage(Map<String, Object> params)
    {
        IPage<CouponEntity> page = this.page(
            new Query<CouponEntity>().getPage(params),
            new QueryWrapper<>()
        );

        return new PageUtils(page);
    }
}
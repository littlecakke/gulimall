package com.atguigu.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.coupon.entity.HomeSubjectSpuEntity;

import java.util.Map;

/**
 * 专题商品
 *
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:09:59
 */
public interface HomeSubjectSpuService extends IService<HomeSubjectSpuEntity>
{
    PageUtils queryPage(Map<String, Object> params);
}
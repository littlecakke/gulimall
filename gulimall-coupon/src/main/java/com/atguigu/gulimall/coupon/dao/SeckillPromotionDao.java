package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:09:59
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity>
{
}
package com.atguigu.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.coupon.entity.SeckillSkuNoticeEntity;

import java.util.Map;

/**
 * 秒杀商品通知订阅
 *
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:09:59
 */
public interface SeckillSkuNoticeService extends IService<SeckillSkuNoticeEntity>
{
    PageUtils queryPage(Map<String, Object> params);
}
package com.atguigu.common.exception;

import lombok.Getter;

/**
 * @author GRS
 * @since 2023/9/24 10:36
 */
@Getter
public enum BizCodeEnum
{
    UNKNOWN_EXCEPTION(10000, "系统未知异常"),
    VALID_EXCEPTION(10001, "参数格式校验失败");

    private Integer code;
    private String msg;

    BizCodeEnum(Integer code, String msg)
    {
        this.code = code;
        this.msg = msg;
    }
}
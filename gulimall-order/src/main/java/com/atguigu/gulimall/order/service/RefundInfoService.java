package com.atguigu.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.order.entity.RefundInfoEntity;

import java.util.Map;

/**
 * 退款信息
 *
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:07:40
 */
public interface RefundInfoService extends IService<RefundInfoEntity>
{
    PageUtils queryPage(Map<String, Object> params);
}
package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.RefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 * 
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:07:40
 */
@Mapper
public interface RefundInfoDao extends BaseMapper<RefundInfoEntity>
{
}
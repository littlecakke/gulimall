package com.atguigu.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.ware.entity.PurchaseDetailEntity;

import java.util.Map;

/**
 * 
 *
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:13:16
 */
public interface PurchaseDetailService extends IService<PurchaseDetailEntity>
{
    PageUtils queryPage(Map<String, Object> params);
}
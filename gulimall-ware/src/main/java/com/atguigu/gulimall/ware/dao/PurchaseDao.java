package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author GRS
 * @email 1245089481@qq.com
 * @since 2023-09-10 14:13:16
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity>
{
}